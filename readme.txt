/*
	All Syntax is identical to the Zurb Foundations main grid features.

	Obviously this is not Zurb and some features are not in the framework but the main pages you will want to look at are:

	Grid: - http://foundation.zurb.com/sites/docs/v/5.5.3/components/grid.html

	Block Grids - http://foundation.zurb.com/sites/docs/v/5.5.3/components/block_grid.html

	Visibility - http://foundation.zurb.com/sites/docs/v/5.5.3/components/visibility.html


	To customise this SASS framework, you will need to edit he _config.scss:

	///
	// SASS SETTINGS PAGE
	//////////////////////

		///
		// MAJOR SETTINGS
		///////////////////
			$breakpoints (key: value, key2: value2, key3: value3)
				Add a key value pair, in size order to add new grid sizes

			$adaptive (true||flase)
				Switch between fluid and adaptive layouts for the .row CSS class


		///
		// GRID SETTINGS
		/////////////////
			$grid (true||false)
				whether or not to output the grid css

			$non-media_grids(true||false)
				whether or not to output the non-media query based grid css

			$media_grids(true||false)
				whether or not to output the media query grid css

			$grid__column-count (INT)
				The amount of columns you want in your grid

			$grid__gutter-width (INT w/unit)
				The gutter size that will be outputted in ratio to the column width

			$grid__column-width (int w/unit)
				The gutter size that will be outputted in ratio to the gutter width

			$row-padding (CSS padding value)
				If you want to add padding to the rows, this is where to add it


		///
		// BLOCK GRID SETTINGS
		///////////////////////
			$block-grid (true||false)
				whether or not to output the block grid css

			$include-spacing (true||false)
				add the defined spacing to individual blocks, or not

			$spacing (CSS padding value)
				the css value of the desired padding


		///
		// VISIBILITY SETTINGS
		///////////////////////
			$visibility-for (true||false)
				output the css for visibility


		///
		// ADDITIONAL POSITIONING 
		//////////////////////////
			$m-push-pull (true||false)
				output css for media based push and pulls

			$nm-push-pull (true||false)
				output css for non-media based push and pulls

			$centered (true||false)
				output css for media based centering queries


*/







